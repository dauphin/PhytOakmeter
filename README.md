# Getting environmental data for association studies

This repository provides codes and explanations on how to use environmental data for association studies.

It covers these topics:

* Get a background map and project it
* Get occurrence data and species range
* Download environmental data
* Extract environmental data from occurrences
* Crop layers for EU extent
* Visualise environmental data and explore ecological gradients
* Sampling locations, data extraction and visualisation
* Principal component analysis of environmental data at the sampling locations
* Correlation plot of environmental data at the sampling locations
* Pearson correlation matrix of environmental data at the sampling locations
* Retain uncorrelated variables based on Pearson's correlation coefficients and make a PCA

# Support

Send an email to benjamin.dauphin@wsl.ch with a reproducible example if a problem occurs when running the script(s).

# Roadmap

Future work will focus on developing codes and tools to assemble comprehensive environmental datasets in the framework of PhytOakmeter.

# Contributing

Any contribution or suggestion to improve or supplement this GitLab project is welcome. Please contact the project maintainer or open an issue to discuss what you would like to update or change.

# License

GNU AGPLv3.

# Project status

This GitLab project is primarily used for teaching purposes in the framework of the Research Unit PhytOakmeter and is therefore updated on a non-regular basis.
